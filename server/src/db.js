import jsonfile from 'jsonfile';

const storagePath = `${__dirname}/storage.json`;

// implemented to be synchronous just to save time
class ProductsDb {
	constructor() {
    this.storage = {"products": []};
	}

	createProduct(product) {
		this.retrieveStorage();
		this.storage.products.push(product);
		this.writeStorage();
	}

	retrieveStorage() {
    this.storage = jsonfile.readFileSync(storagePath);
	}

  writeStorage() {
    jsonfile.writeFileSync(storagePath, this.storage);
  }

  getProducts() {
		this.retrieveStorage();
		return this.storage.products;
  }
}

export default callback => {
	// connect to a database if needed, then pass it to `callback`:
	callback(new ProductsDb());
}
