import http from 'http';
import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import initializeDb from './db';
import api from './api';
import config from './config.json';

let app = express();
app.server = http.createServer(app);

app.use(morgan('dev'));

app.use(bodyParser.json());

app.use(express.static(`${__dirname}/../../client/build`));

// connect to db
initializeDb( db => {
	// api router
	app.use('/api', api({ config, db }));

	app.server.listen(process.env.PORT || config.port, () => {
		console.log(`Started on port ${app.server.address().port}`);
	});
});

export default app;
