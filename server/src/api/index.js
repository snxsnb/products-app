import { Router } from 'express';
import products from './products';

export default ({ config, db }) => {
	let api = Router();

	api.use('/products', products({ config, db }));

	api.get('*', (req, res) => {
		res.sendFile(`${__dirname}/../../client/build`);
	});

	return api;
}
