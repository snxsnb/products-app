import resource from 'resource-router-middleware';

export default ({ config, db }) => resource({
	/** GET / - List all entities */
  index({params}, res) {
    res.json(db.getProducts());
  },

  /** POST / - Create a new entity */
  create({body}, res) {
    let id = db.getProducts().length.toString(36);
    body.id = id;
    db.createProduct(body);
    res.json(body);
  },
});
