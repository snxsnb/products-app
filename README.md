### How to build and run ###

1. Build client: `cd client` then `npm i` and finally `npm run build`
2. Prepare server: `cd ../server` then `npm i`
3. Run node server: `npm start`
4. Open in browser http://localhost:8080