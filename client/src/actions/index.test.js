import * as actions from './index';
import { ActionType } from '../enums/enums'

describe('product actions', () => {
    it('addProduct should create ADD_PRODUCT action', () => {
        expect(actions.addProduct('iphone', 'red')).toEqual({
            type: ActionType.ADD_PRODUCT,
            id: 0,
            name: 'iphone',
            color: 'red'
        });
    });

    it('deleteProduct should create DELETE_PRODUCT action', () => {
        expect(actions.deleteProduct('1')).toEqual({
            type: ActionType.DELETE_PRODUCT,
            id: 1
        });
    });
});