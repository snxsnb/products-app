import { ActionType } from '../enums/enums';

export const addProduct = (name, color) => ({
    type: ActionType.ADD_PRODUCT,
    name: name,
    color: color
});

export const getProducts = () => ({
  type: ActionType.GET_PRODUCTS
});

export const getProductsFulfilled = (data) => ({
  type: ActionType.GET_PRODUCTS_FULFILLED,
  data: data
});