import {ActionType} from '../enums/enums';

export default (state = [], action) => {
  switch (action.type) {
    case ActionType.ADD_PRODUCT:
      return [
        ...state,
        {
          id: action.id,
          name: action.name,
          color: action.color
        }
      ];
    case ActionType.GET_PRODUCTS:
      return state;
    case ActionType.GET_PRODUCTS_FULFILLED:
      return action.data;
    default:
      return state;
  }
};