export const ActionType = {
  ADD_PRODUCT: 'ADD_PRODUCT',
  DELETE_PRODUCT: 'DELETE_PRODUCT',
  GET_PRODUCTS: 'GET_PRODUCTS',
  GET_PRODUCTS_FULFILLED: 'GET_PRODUCTS_FULFILLED'
};

export const ProductColor = {
  RED: 'red',
  GREEN: 'green',
  BLUE: 'blue'
};