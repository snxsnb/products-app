import { combineEpics } from 'redux-observable';
import getProductsEpic from './getProductsEpic';
import addProductEpic from './addProductEpic';

export default combineEpics(
  getProductsEpic,
  addProductEpic
);