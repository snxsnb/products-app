import { ActionType } from '../enums/enums';
import { getProductsFulfilled } from '../actions';
import { ajax } from 'rxjs/observable/dom/ajax';

export default function getProductsEpic(action$) {
  return action$.ofType(ActionType.GET_PRODUCTS)
    .mergeMap(action => ajax.getJSON('/api/products').map(getProductsFulfilled)
    );
};