import { ActionType } from '../enums/enums';
import { getProducts } from '../actions';
import { ajax } from 'rxjs/observable/dom/ajax';

export default function addProductEpic(action$) {
  return action$.ofType(ActionType.ADD_PRODUCT)
    .mergeMap(action =>
      ajax.post('/api/products', {name: action.name, color: action.color}, {'Content-Type': 'application/json'})
        .map(getProducts));
};