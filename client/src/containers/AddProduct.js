import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addProduct } from '../actions';
import { ProductColor } from '../enums/enums';
import Input from 'react-toolbox/lib/input/Input';
import Button from 'react-toolbox/lib/button/Button';
import Dropdown from 'react-toolbox/lib/dropdown/Dropdown';

import  './AddProduct';

class AddProduct extends Component {
  constructor() {
    super();
    this.state = {name: '', color: '', nameError: null};
  }

  validateNameErrors(inputValue) {
    let isValid = typeof(inputValue) === 'string' && inputValue.length >= 4 && inputValue.length <= 8;
    if (!isValid) {
      return 'Product name should consist of 4-8 symbols';
    }

    return null;
  }

  submitForm(e) {
    const { dispatch } = this.props;

    e.preventDefault();
    if (this.validateNameErrors(this.state.name) || !this.state.color) {
      return;
    }

    dispatch(addProduct(this.state.name, this.state.color));
    this.resetFormData();
  }

  handleChange(name, value) {
    if (name === 'name') {
      this.setState({nameError: this.validateNameErrors(value)});
    }

    this.setState({[name]: value});
  };

  resetFormData() {
    this.setState({name: '', color: null});
  }

  getColorSource() {
    let source = [];
    for (let key in ProductColor) {
      source.push({value: ProductColor[key], label: ProductColor[key]});
    }

    return source;
  }

  render() {
    return (
      <div>
        <form className="product-form" onSubmit={e => this.submitForm(e)}>
          <div className="product-form__row">
            <Input type="text" label="Product name" value={this.state.name}
              onChange={this.handleChange.bind(this, 'name')} error={this.state.nameError} required/>
          </div>
          <div className="product-form__row">
            <Dropdown label="Product color" source={this.getColorSource()} value={this.state.color}
              onChange={this.handleChange.bind(this, 'color')} required/>
          </div>
          <div className="product-form__row">
            <Button type="submit" raised>Add Product</Button>
          </div>
        </form>
      </div>
    );
  };
}

AddProduct = connect()(AddProduct);

export default AddProduct;
