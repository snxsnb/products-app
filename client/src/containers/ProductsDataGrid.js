import { connect } from 'react-redux';
import ProductList from '../components/productList/ProductList';

const mapStateToProps = (state) => ({
  products: state
});

const ProductsDataGrid = connect(mapStateToProps)(ProductList);
export default ProductsDataGrid;