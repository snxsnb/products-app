import React, { Component } from 'react';
import { connect } from 'react-redux';
import AddProduct from '../../containers/AddProduct';
import ProductsDataGrid from "../../containers/ProductsDataGrid";
import theme from '../../assets/react-toolbox/theme';
import ThemeProvider from 'react-toolbox/lib/ThemeProvider';
import { getProducts } from '../../actions';

import './App.css';

class App extends Component {
  componentDidMount() {
    this.props.dispatch(getProducts());
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <div className="app">
          <div className="app-header">
            <h2>Products App</h2>
          </div>
          <div className="app-content">
            <div className="app-content__add-form">
              <AddProduct />
            </div>
            <div className="app-content__product-list">
              <ProductsDataGrid />
            </div>
          </div>
        </div>
      </ThemeProvider>
    );
  }
}

App = connect()(App);
export default App;
