import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Product.css';

class Product extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired
  };

  render() {
    const { name, color } = this.props;
    return (
      <div className="product">
        <div className="product-name">{name}</div>
        <div className="product-color">{color}</div>
      </div>
    );
  }
}

export default Product;