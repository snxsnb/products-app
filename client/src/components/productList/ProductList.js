import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Product from '../product/Product';
import './ProductList.css';

class ProductList extends Component {
  static propTypes = {
    products: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      color: PropTypes.string.isRequired
    }).isRequired).isRequired
  };

  render() {
    const { products } = this.props;
    return (        
      <div className="product-list">
        <div className="product-list__header">
          <div className="product-list__header-item">Name</div>
          <div className="product-list__header-item">Color</div>
        </div>
        {products.map((product) => <Product key={product.id} {...product}/>)}
      </div>
    );
  }
}

export default ProductList;